rm -rf /opt/ANDRAX/gtp_scan

source /opt/ANDRAX/PYENV/python2/bin/activate

/opt/ANDRAX/PYENV/python2/bin/pip2 install IPy

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip install... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf gtp-scan.py /opt/ANDRAX/PYENV/python2/bin/

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
